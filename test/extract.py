# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 12:14:34 2020

@author: yboge
"""


# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 13:50:55 2020
@author: Yoann Boget
"""
import pandas as pd
import os
from smart_sensor_client.smart_sensor_client import SmartSensorClient
import re
import numpy as np
import re
import datetime as dt

DEFAULT_SETTINGS_FILE = 'settings.yaml'
pd.set_option('display.max_rows', 20)
pd.set_option('display.max_columns', 10)

os.getcwd()
os.chdir('C:/Users/yboge/PyCharmProjects/ABB')

def authenticate(settings_file=DEFAULT_SETTINGS_FILE):
    # Create the client instance

    client = SmartSensorClient(settings_file=settings_file)

    # Authenticate
    if not client.authenticate():
        print('Authentication FAILED')
        return False
    return client


def getPlants(client):
    # Print organization
    print('Organization {}, {}'.format(client.organization_id, client.organization_name))
    print()
    # Get list of plants
    plants = client.get_plant_list()
    # Iterate the plant list
    for plant in plants:
        print('Plant {}, {}:'.format(plant['plantID'], plant['plantName']))
    return plants

def getAssets(plants):
    assets = []
    for plant in plants:
        # Get list of assets
        assets.extend(client.get_asset_list(organization_id=client.organization_id, plant_id=plant['plantID']))
    for asset in assets:
        print('Asset: {}, {}: {}'.format(asset['assetID'], asset['assetName'], asset['assetType']))
    return assets


def getMeasurementNames(assets):
    # Return two lists: one with of unique measurement ID the other with unique measurement Name
    measurement_list = []
    measurement_typeID_list = []
    for asset in assets:
        asset_data = client.asset_get_asset_by_id(asset_id=asset['assetID'])
        # Iterate all the measurements available and save unique entry

        for m in asset_data['measurements']:
            if m['measurementTypeID'] not in measurement_typeID_list:
                measurement_typeID_list.append(m['measurementTypeID'])
                measurement_list.append(m['measurementTypeName'])
        print(measurement_typeID_list)
        print(measurement_list)
        print(asset_data['measurements'])
    return measurement_list


def createDataFrame(assets):
    i = 0
    m_t = 0
    colomns_name=['assetID', 'measureTypeName', 'measureTypeID', 'measurementsValue', 'measurementCreated']
    df = pd.DataFrame(columns=colomns_name)
    for asset in assets:
        while m_t <= 300:
            asset_data = client.get_measurement_value(asset_id=asset['assetID'], measurement_type=m_t,
                                                      start_time='2000-01-01T00:00:00', end_time='2020-12-31T23:59:59')
            print(m_t)
            if not asset_data:
                # If there is an error, skip to the next asset
                m_t=m_t+1
                continue
            if asset_data[0]['measurements'][0]['measurementValue'] is not None:
                assetID=asset['assetID']
                measureTypeName=asset_data[0]['measurementTypeName']
                measureTypeID = asset_data[0]['measurementTypeID']
                for measurements in asset_data[0]['measurements']:
                    measurementsValue=measurements['measurementValue']
                    measurementCreated=measurements['measurementCreated']
                    #add line to dataframe
                    new_row= [assetID, measureTypeName, measureTypeID, measurementsValue, measurementCreated]
                    row_df = pd.DataFrame([new_row], index=[i], columns=colomns_name)
                    df = pd.concat([df, row_df])
                    i += 1
                df.to_pickle("data/ABB1_{}_{}.pkl".format(assetID, measureTypeID))
                df = pd.DataFrame(columns=colomns_name)
            print('end_of {} {}'.format(assetID, m_t))
            m_t = m_t+1
            
        m_t = 0
        print('new_asset')

def get_measurement_info(requested_info, data, client):
    # 1) Info is a string for the type of information about \
    #    the measurement. It should be the same for each measurementType
    # 2) MeasurementTypeIDs is a list with the measurements IDs
    # 3) assets is a list of assets where the measurements appear
    #
    # NB: Created to obtain 'measurementTypeDescription'
    info_list = []
    j=0
    for i in range(2, len(data.columns)):
        ID = re.findall(r'ID: (.*?)\)', data.columns[i])[0]
        measurementTypeID=np.float(ID)
        while pd.isna(data.iloc[j, i]):
            j+=1
        assetID = data.iloc[j, 0]
        asset_data = client.asset_get_asset_by_id(asset_id=np.int(assetID))

        found=False
        if not asset_data:
                # If there is an error, skip to the next asset
            continue
        for m in range(len(asset_data['measurements'])):
            if asset_data['measurements'][m]['measurementTypeID']== measurementTypeID:
                print(asset_data['measurements'][m][requested_info])
                info_list.append([measurementTypeID, asset_data['measurements'][m][requested_info]])
                found=True
                break
            if found:
                break
    return info_list



DEFAULT_SETTINGS_FILE = 'settings.yaml'
authenticate(settings_file=DEFAULT_SETTINGS_FILE)
client = authenticate(DEFAULT_SETTINGS_FILE)
plants = getPlants(client)
assets = getAssets(plants)
createDataFrame(assets)
    

