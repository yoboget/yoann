import pandas as pd
import numpy as np


def raw_data_to_dataset(datapath):
    raw_data=pd.read_pickle(datapath)
    columns=[0] + list(range(2,5)) + list(range(7, 13)) + list(range(15, 30))
    data=raw_data.iloc[:,columns]
    data=data.dropna()
    return data.to_numpy()

def dataset_to_training(data):
    X= np.delete(data, 5, axis=1)
    print(X.shape)
    Y= data[:,5]
    perm= np.random.permutation(np.arange(len(Y)))
    Xperm=X[perm,:]
    Yperm=Y[perm]
    split=int(np.round(len(Y)*0.7, 0))
    X_train=Xperm[:split, :]
    Y_train=Yperm[:split]
    X_test=Xperm[split:, :]
    Y_test=Yperm[split:]
    return X_train, Y_train, X_test, Y_test

def panda_to_numpy_float(pandas_file_path):
    data=raw_data_to_dataset(pandas_file_path)
    numpy=np.zeros(data.shape)
    for i in range(len(data[:, 0])):
        for j in range(len(data[0, :])):
            numpy[i,j]=np.double(data[i,j])
    np.save('data.npy', numpy)


