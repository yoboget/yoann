from SimpleNN import Regression
class argstuff(object):
    def __init__(self, epoch=10000, batch_size=32, output_dir="results",
                 lr=0.0001, beta1=0.9, beta2=0.999, gpu_mode = False):
        self.epoch = epoch
        self.batch_size = batch_size
        self.output_dir = output_dir
        self.lr = lr
        self.beta1 = beta1
        self.beta2 = beta2
        self.gpu_mode = gpu_mode

args = argstuff()
SimpleNN = Regression(args)
SimpleNN.train()