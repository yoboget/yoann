import numpy as np
import torch as to
import torch.nn as nn
import torch.optim as optim
import time
import matplotlib.pyplot as plt
from IPython import display
from sklearn.preprocessing import minmax_scale
from Preprocessing import dataset_to_training
from utils import print_network, initialize_weights


class MLP_Reg(nn.Module):
    def __init__(self):
        super(MLP_Reg, self).__init__()
        self.input_dim = 24
        self.output_dim = 1
        self.fc = nn.Sequential(
            nn.Dropout(0.1),
            nn.Linear(self.input_dim, 1024),
            nn.LeakyReLU(0.2),
            nn.BatchNorm1d(1024),
            nn.Dropout(0.1),
            nn.Linear(1024, 512),
            nn.LeakyReLU(0.2),
            nn.BatchNorm1d(512),
            nn.Dropout(0.1),
            nn.Linear(512, 256),
            nn.LeakyReLU(0.1),
            nn.BatchNorm1d(256),
            nn.Dropout(0.1),
            nn.Linear(256, 125),
            nn.LeakyReLU(0.2),
            nn.BatchNorm1d(125),
            nn.Dropout(0.1),
            nn.Linear(125, self.output_dim),
        )

        initialize_weights(self)

    def forward(self, input):
        x = self.fc(input)
        return x


class Regression(object):
    def __init__(self, args):
        # parameters
        self.epoch = args.epoch
        self.batch_size = args.batch_size
        self.output_dir = args.output_dir
        self.lr = args.lr
        self.beta1 = args.beta1
        self.beta2 = args.beta2
        self.gpu_mode = args.gpu_mode
        self.C = MLP_Reg()

        self.optimizer = optim.Adam(self.C.parameters(), lr=args.lr, betas=(args.beta1, args.beta2))
        self.scheduler = optim.lr_scheduler.StepLR(self.optimizer, step_size=1, gamma=0.9)
        if self.gpu_mode:
            self.MSELoss = nn.MSELoss().cuda()
        else:
            self.MSELoss = nn.MSELoss()

        print('---------- Networks architecture -------------')
        print_network(self.C)
        print('-----------------------------------------------')

        # load, scale and split data
        self.data = np.load('data.npy')
        self.X_train, self.Y_train, self.X_test, self.Y_test = dataset_to_training(self.data)
        self.X_train, self.X_test = minmax_scale(self.X_train), minmax_scale(self.X_test)
        self.X_train = to.from_numpy(self.X_train).float()
        self.Y_train = to.from_numpy(self.Y_train).float()
        self.X_test = to.from_numpy(self.X_test).float()
        self.Y_test = to.from_numpy(self.Y_test).float()

        # print("data X")
        # print(self.data_X)
        # print("data Y")
        # print(self.data_Y)

    def train(self):
        self.train_hist = {}
        self.train_hist['loss_train'] = []
        self.train_hist['loss_test'] = []
        self.train_hist['Mean_loss_test'] = []
        self.train_hist['Mean_loss_train'] = []

        if self.gpu_mode:
            [i.cuda() for i in [self.X_train, self.Y_train, self.X_test, self.Y_test]]

        self.C.train()
        self.batch_size_test = int(np.floor(self.batch_size * 0.3 / 0.7))
        print('training start!!')
        start_time = time.time()
        for epoch in range(self.epoch):
            epoch_start_time = time.time()
            perm= np.random.permutation(len(self.Y_train))
            self.X_train=self.X_train[perm, :]
            self.Y_train=self.Y_train[perm]
            perm = np.random.permutation(len(self.Y_test))
            self.X_test = self.X_test[perm, :]
            self.Y_test = self.Y_test[perm]
                
            for iter in range(len(self.X_train) // self.batch_size):
                self.C.train()
                self.optimizer.zero_grad()
                x_ = self.X_train[iter * self.batch_size:iter * self.batch_size + self.batch_size]
                y_ = self.Y_train[iter * self.batch_size:iter * self.batch_size + self.batch_size]
                x_test = self.X_test[iter * self.batch_size_test:iter * self.batch_size_test + self.batch_size_test]
                y_test = self.Y_test[iter * self.batch_size_test:iter * self.batch_size_test + self.batch_size_test]
                if self.gpu_mode:
                    x_, y_, = x_.cuda(), y_.cuda()
                self.yhat = to.squeeze(self.C(x_))
                loss = self.MSELoss(self.yhat, y_)
                
                loss.backward()
                self.optimizer.step()
                self.train_hist['loss_train'].append(loss.detach().numpy())
                
                self.C.eval()
                self.yhat = to.squeeze(self.C(x_test))
                loss_test = self.MSELoss(self.yhat, y_test)
                self.train_hist['loss_test'].append(loss_test.detach().numpy())
                
                if iter % 500 ==0:
                    self.train_hist['Mean_loss_train'].append(np.mean(self.train_hist['loss_train']))
                    self.train_hist['Mean_loss_test'].append(np.mean(self.train_hist['loss_test']))
                    self.train_hist['loss_train'] = []
                    self.train_hist['loss_test'] = []
                    print(iter/(len(self.X_train) // self.batch_size))
                    
                    yhat = to.squeeze(self.C(self.X_test[self.Y_test>3][:50,:]))

            print(np.concatenate((self.Y_test[self.Y_test>3][:50].unsqueeze(1).detach().numpy(), 
                                  np.round(yhat.unsqueeze(1).detach().numpy())), axis=1))
                    
                    
            self.scheduler.step()
            print(self.optimizer.param_groups[0]['lr'])
            print(epoch+1)
           

            if ((epoch + 1) % 1) == 0:
                loss = range(len(self.train_hist['Mean_loss_train']))
                loss_test = range(len(self.train_hist['Mean_loss_test']))
                display.clear_output(wait=True)
                display.display(plt.gcf())

                plt.plot(loss, self.train_hist['Mean_loss_train'], ms=0.1, label="Training")
                plt.plot(loss_test, self.train_hist['Mean_loss_test'], ms=0.1, label="Test")
                plt.ylim([0, 1])
                print(self.train_hist['Mean_loss_test'])
                plt.legend()
                plt.show()


