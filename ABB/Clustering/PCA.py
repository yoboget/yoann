# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 09:24:49 2020

@author: yboge
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

from sklearn.decomposition import PCA
from sklearn.preprocessing import minmax_scale



data = np.load('data.npy')
X = data[:,2:]
Y = data[:,0] 
data = minmax_scale(X)
pca=PCA(n_components=3)
Xpca = pca.fit_transform(X)

selection=np.random.choice(X.shape[0], 10000, replace=False)

Xpca_sel=Xpca[selection, :]
Y_sel=Y[selection]

targets= np.unique(Y_sel)

viridis = cm.get_cmap('viridis', len(targets))



for i in range(len(targets)):

    fig = plt.figure(figsize = (8,8))
    ax = fig.add_subplot(1,1,1) 
    ax.set_xlabel('Principal Component 1', fontsize = 15)
    ax.set_ylabel('Principal Component 2', fontsize = 15)
    ax.set_title('2 component PCA', fontsize = 20)
    plt.scatter(Xpca_sel[Y_sel==targets[i],0], Xpca_sel[Y_sel==targets[i],1])
    plt.show()

