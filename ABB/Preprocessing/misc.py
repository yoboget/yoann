

# anaconda3\Lib\site-packages\smart-sensor-api-client\use_cases
# '~/anaconda3/Lib/site-packages/smart-sensor-api-client/settings.yaml'

import pandas as pd
import datetime as dt
import numpy as np
import os

def dataStringToIntegers(dataString):
    year=np.int(dataString[0:4])
    month=np.int(dataString[5:7])
    day=np.int(dataString[8:10])
    hour=np.int(dataString[11:13])
    minute=np.int(dataString[14:16])
    sec= np.int(dataString[17:19])
    return year, month, day, hour, minute, sec

def dataStringToDatetime(dataString):
    year=np.int(dataString[0:4])
    month=np.int(dataString[5:7])
    day=np.int(dataString[8:10])
    hour=np.int(dataString[11:13])
    minute=np.int(dataString[14:16])
    sec= np.int(dataString[17:19])
    datetime= dt.datetime(year, month, day, hour, minute, sec)
    return datetime

def not_all_nans(data, columns):
    # return the lines where not all columns(columns numbers) are nans
    return data.iloc[np.where(np.sum((~pd.isna(data.iloc[:,columns]))*1, axis=1)> 0)]

