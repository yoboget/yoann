# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 13:50:55 2020
@author: Yoann Boget
"""
import pandas as pd
import os
from smart_sensor_client.smart_sensor_client import SmartSensorClient
import re
import numpy as np
import re
import datetime as dt

DEFAULT_SETTINGS_FILE = 'settings.yaml'
pd.set_option('display.max_rows', 20)
pd.set_option('display.max_columns', 10)

os.getcwd()
os.chdir('C:/Users/yboge/PyCharmProjects/ABB')

def authenticate(settings_file=DEFAULT_SETTINGS_FILE):
    # Create the client instance

    client = SmartSensorClient(settings_file=settings_file)

    # Authenticate
    if not client.authenticate():
        print('Authentication FAILED')
        return False
    return client


def getPlants(client):
    # Print organization
    print('Organization {}, {}'.format(client.organization_id, client.organization_name))
    print()
    # Get list of plants
    plants = client.get_plant_list()
    # Iterate the plant list
    for plant in plants:
        print('Plant {}, {}:'.format(plant['plantID'], plant['plantName']))
    return plants

def getAssets(plants):
    assets = []
    for plant in plants:
        # Get list of assets
        assets.extend(client.get_asset_list(organization_id=client.organization_id, plant_id=plant['plantID']))
    for asset in assets:
        print('Asset: {}, {}: {}'.format(asset['assetID'], asset['assetName'], asset['assetType']))
    return assets


def getMeasurementNames(assets):
    # Return two lists: one with of unique measurement ID the other with unique measurement Name
    measurement_list = []
    measurement_typeID_list = []
    for asset in assets:
        asset_data = client.asset_get_asset_by_id(asset_id=asset['assetID'])
        # Iterate all the measurements available and save unique entry

        for m in asset_data['measurements']:
            if m['measurementTypeID'] not in measurement_typeID_list:
                measurement_typeID_list.append(m['measurementTypeID'])
                measurement_list.append(m['measurementTypeName'])
        print(measurement_typeID_list)
        print(measurement_list)
        print(asset_data['measurements'])
    return measurement_list


def createDataFrame(assets):
    i = 0
    m_t = 0
    colomns_name=['assetID', 'measureTypeName', 'measureTypeID', 'measurementsValue', 'measurementCreated']
    df = pd.DataFrame(columns=colomns_name)
    for asset in assets:
        while m_t <= 300:
            asset_data = client.get_measurement_value(asset_id=asset['assetID'], measurement_type=m_t,
                                                      start_time='2000-01-01T00:00:00', end_time='2020-12-31T23:59:59')
            print(m_t)
            if not asset_data:
                # If there is an error, skip to the next asset
                m_t=m_t+1
                continue
            if asset_data[0]['measurements'][0]['measurementValue'] is not None:
                assetID=asset['assetID']
                measureTypeName=asset_data[0]['measurementTypeName']
                measureTypeID = asset_data[0]['measurementTypeID']
                for measurements in asset_data[0]['measurements']:
                    measurementsValue=measurements['measurementValue']
                    measurementCreated=measurements['measurementCreated']
                    #add line to dataframe
                    new_row= [assetID, measureTypeName, measureTypeID, measurementsValue, measurementCreated]
                    row_df = pd.DataFrame([new_row], index=[i], columns=colomns_name)
                    df = pd.concat([df, row_df])
                    i += 1
                df.to_pickle("data/ABB1_{}_{}.pkl".format(assetID, measureTypeID))
                df = pd.DataFrame(columns=colomns_name)
            m_t = m_t+1
            print('end_of {} {}'.format(assetID, m_t))
        m_t = 0
        print('new_asset')

def get_measurement_info(requested_info, data, client):
    # 1) Info is a string for the type of information about \
    #    the measurement. It should be the same for each measurementType
    # 2) MeasurementTypeIDs is a list with the measurements IDs
    # 3) assets is a list of assets where the measurements appear
    #
    # NB: Created to obtain 'measurementTypeDescription'
    info_list = []
    j=0
    for i in range(2, len(data.columns)):
        ID = re.findall(r'ID: (.*?)\)', data.columns[i])[0]
        measurementTypeID=np.float(ID)
        while pd.isna(data.iloc[j, i]):
            j+=1
        assetID = data.iloc[j, 0]
        asset_data = client.asset_get_asset_by_id(asset_id=np.int(assetID))

        found=False
        if not asset_data:
                # If there is an error, skip to the next asset
            continue
        for m in range(len(asset_data['measurements'])):
            if asset_data['measurements'][m]['measurementTypeID']== measurementTypeID:
                print(asset_data['measurements'][m][requested_info])
                info_list.append([measurementTypeID, asset_data['measurements'][m][requested_info]])
                found=True
                break
            if found:
                break
    return info_list


def merge_data_frame(dir_from, dir_to, assetID=None):
    dataframe = pd.DataFrame()
    for filename in [f for f in os.listdir(dir_from) 
                     if os.path.isfile('{}{}'.format(dir_from, f))]:
        assetIDfilename=re.findall(r'_(.*?)_', filename)
        if int(assetIDfilename[0])==assetID:
            if dataframe.empty:
                dataframe=pd.read_pickle('{}{}'.format(dir_from, filename))
            df=pd.read_pickle('{}{}'.format(dir_from, filename))
            dataframe = pd.concat([dataframe, df])
    pd.to_pickle(dataframe, '{}/ABB1_{}.pkl'.format(dir_to, assetID))
    return dataframe
      



def transform_dataframe(asset):
    df= pd.read_pickle('data/data_by_asset/ABB1_{}.pkl'.format(asset))
    uniqueID=np.unique(df['measureTypeID'])
    columnsName=df[['measureTypeName', 
                'measureTypeID']][df['measureTypeID']==uniqueID[0]].to_numpy()[0]
    left = df[['assetID','measurementCreated', 
               'measurementsValue']][df['measureTypeID']==uniqueID[0]]
    left=left.rename(columns={'measurementsValue': 
                    '{} (ID: {})'.format(columnsName[0],columnsName[1])})
    for ID in range(1, len(uniqueID)):
        columnsName=df[['measureTypeName', 
            'measureTypeID']][df['measureTypeID']==uniqueID[ID]].to_numpy()[0]
        right = df[['assetID','measurementCreated', 
            'measurementsValue']][df['measureTypeID']==uniqueID[ID]]
        right = right.rename(columns={'measurementsValue': 
            '{} (ID: {})'.format(columnsName[0],columnsName[1])})
        left = pd.merge(left, right, how='outer', on=['assetID', 'measurementCreated'])
    return left


def merge_data_frame2(dir_from, dir_to):
    dataframe = pd.DataFrame()
    for filename in [f for f in os.listdir(dir_from) 
                     if os.path.isfile('{}{}'.format(dir_from, f)) 
                     and f[0:5]=='Trend']: 
        if dataframe.empty:
            dataframe=pd.read_pickle('{}{}'.format(dir_from, filename))
        df=pd.read_pickle('{}{}'.format(dir_from, filename))
        dataframe = pd.concat([dataframe, df], sort=False)
    pd.to_pickle(dataframe, 'DATA.pkl')
    return dataframe

def data_string_to_float(data, columns):
    dataNP = data.to_numpy()
    for j in columns:
        for i in range(0, len(dataNP[:, j])):
            if np.isnan(np.float(dataNP[i, j])):
                continue
            else:
                dataNP[i, j] = np.float(dataNP[i, j])
    columns=data.columns
    DATA= pd.DataFrame(dataNP, columns=columns)
    pd.to_pickle(DATA, 'DataFloat.pkl')
    return DATA


import pandas as pd
import numpy as np
import os
from misc import dataStringToDatetime
def data_string_to_datatime(data, columns):
    dataNP = data.to_numpy()
    for j in columns:
        for i in range(0, len(dataNP[:, j])):
            if pd.isna(dataNP[i, j]):
                continue
            else:
                dataNP[i, j] = dataStringToDatetime(dataNP[i, j])
    columns=data.columns
    DATA= pd.DataFrame(dataNP, columns=columns)
    pd.to_pickle(DATA, 'DataClean.pkl')
    return DATA



import pandas as pd
import numpy as np
def data_grouping(data, columns):
    dataset = data.iloc[np.where(np.sum((~pd.isna(data.iloc[:,columns]))*1, axis=1)> 0)]
    pd.to_pickle(dataset, 'Datagroup2.pkl')
    return data


DEFAULT_SETTINGS_FILE = 'settings.yaml'
authenticate(settings_file=DEFAULT_SETTINGS_FILE)
client = authenticate(DEFAULT_SETTINGS_FILE)
data= pd.read_pickle('DataClean.pkl')
get_measurement_info('measurementTypeDescription', data, client)


#getMeasurementInfo('measurementTypeDescription', uniqueID, assets)


#client = authenticate(DEFAULT_SETTINGS_FILE)
#plants = getPlants(client)
#assets = getAssets(plants)
#print(measureID)
#getMeasurementInfo('measurementTypeDescription', measureID, assets)

        