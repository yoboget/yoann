# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 15:13:05 2020

@author: yboge
"""

import pandas as pd
import numpy as np

data=pd.read_pickle('Dataclean.pkl')
print(data.columns[2])
def explore_dataset(data):
    dataNP = data.to_numpy()
    columns = data.columns
    table=[]

    for j in range(2, len(dataNP[0])):
        # print('Feature: {}    Number of unique value:{}'.format(i, len(np.unique(dataNP[:,i]))))
        # print(np.unique(dataNP[:,i]))
        d = []
        for i in range(0, len(dataNP[:, j])):
            if np.isnan(np.float(dataNP[i, j])):
                continue
            else:
                dataNP[i, j] = np.float(dataNP[i, j])
                d.append(dataNP[i, j])
        line=[columns[j], len(d), len(np.unique(d))]

        print('{}:  N={}   Unique Values={}'.format(columns[j], len(d), len(np.unique(d))))
        if len(np.unique(d)) <= 30:
            print(np.unique(d))
        print()
        table.append(line)
    return table



def corr_nans(col1, col2, data):
    dataNP = data.to_numpy()
    dataNP=dataNP[:, 2:]
    matrix=np.logical_not(pd.isna(dataNP))
    return np.dot(matrix[:, col1]*1, matrix[:, col2]*1)

#for i in range(4, 37):
#    print(corr_nans(3, i, data))

table=explore_dataset(data)
print(table)
