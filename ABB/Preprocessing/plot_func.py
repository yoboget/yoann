# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 10:22:20 2020

@author: yboge
"""


import matplotlib.pyplot as plt
import matplotlib.dates as pltdt
import pandas as pd
from misc import dataStringToDatetime	
pd.set_option('display.max_rows', 20)
pd.set_option('display.max_columns', 5)

def plotRowData(df, ID, range_start, range_end):
    list_datetime=[]
    y_list=[None]*len(ID)
    for i in range(len(ID)):
       
        y_list[i]=df['measurementsValue'][df['measureTypeID']==ID[i]][range_start:range_end]
        print(y_list[i])
    for row in range(len(df['measurementCreated'][range_start:range_end])):
        list_datetime.append(dataStringToDatetime(df['measurementCreated'].iloc[row]))
    dates = pltdt.date2num(list_datetime)
    legend = 'N = {}'.format(len(df['measurementsValue']))
    for i in range(len(y_list)):
        plt.plot_date(dates, 
                  y_list[i], fmt='o', ms=0.3, xdate=True, label= legend)
    plt.ylim(0, 1)
    plt.ylabel('Value')
    plt.legend(loc=1)
    plt.show()

df5545=pd.read_pickle('data/data_by_asset/ABB1_5545')
plotRowData(df5545, [2, 4, 9, 10, 15,  27,  31, 32, 33], 200, 500)