import pandas as pd
import re
import numpy as np
import os
from smart_sensor_client.smart_sensor_client import SmartSensorClient

DEFAULT_SETTINGS_FILE = 'settings.yaml'


# anaconda3\Lib\site-packages\smart-sensor-api-client\use_cases
# '~/anaconda3/Lib/site-packages/smart-sensor-api-client/settings.yaml'


def authenticate(settings_file=DEFAULT_SETTINGS_FILE):
    # Create the client instance

    client = SmartSensorClient(settings_file=settings_file)

    # Authenticate
    if not client.authenticate():
        print('Authentication FAILED')
        return False
    return client


def getPlants(client):
    # Print organization
    print('Organization {}, {}'.format(client.organization_id, client.organization_name))
    print()
    # Get list of plants
    plants = client.get_plant_list()
    # Iterate the plant list
    for plant in plants:
        print('Plant {}, {}:'.format(plant['plantID'], plant['plantName']))
    return plants


def getAssets(plants):
    assets = []
    for plant in plants:
        # Get list of assets
        assets.extend(client.get_asset_list(organization_id=client.organization_id, plant_id=plant['plantID']))
    for asset in assets:
        print('Asset: {}, {}: {}'.format(asset['assetID'], asset['assetName'], asset['assetType']))
    return assets


def getMeasurementNames(assets):
    # Return two lists: one with of unique measurement ID the other with unique measurement Name
    for asset in assets:
        asset_data = client.asset_get_asset_by_id(asset_id=asset['assetID'])
        # Iterate all the measurements available and save unique entry
        measurement_list = []
        measurement_typeID_list = []
        for m in asset_data['measurements']:
            if m['measurementTypeID'] not in measurement_typeID_list:
                measurement_typeID_list.append(m['measurementTypeID'])
                measurement_list.append(m['measurementTypeName'])
        print(measurement_typeID_list)
        print(measurement_list)
        print(asset_data['measurements'])
        return measurement_list


def createDataFrame(assets):
    i = 0
    m_t = 209
    colomns_name=['assetID', 'measureTypeName', 'measureTypeID', 'measurementsValue', 'measurementCreated']
    df = pd.DataFrame(columns=colomns_name)
    dir_from='data/'
    assetID=[]

    for asset in assets:
        if asset['assetID'] == 7221:
            assetID = []

            while m_t <= 300:
                asset_data = client.get_measurement_value(asset_id=asset['assetID'], measurement_type=m_t,
                                                          start_time='2000-01-01T00:00:00', end_time='2020-12-31T23:59:59')
                print(m_t)
                if not asset_data:
                    # If there is an error, skip to the next asset
                    m_t=m_t+1
                    continue
                if asset_data[0]['measurements'][0]['measurementValue'] is not None:
                    assetID=asset['assetID']
                    measureTypeName=asset_data[0]['measurementTypeName']
                    measureTypeID = asset_data[0]['measurementTypeID']
                    for measurements in asset_data[0]['measurements']:
                        measurementsValue=measurements['measurementValue']
                        measurementCreated=measurements['measurementCreated']
                        #add line to dataframe
                        new_row= [assetID, measureTypeName, measureTypeID, measurementsValue, measurementCreated]
                        row_df = pd.DataFrame([new_row], index=[i], columns=colomns_name)
                        df = pd.concat([df, row_df])
                        i += 1
                    df.to_pickle("data/ABB1_{}_{}.pkl".format(assetID, measureTypeID))
                    df = pd.DataFrame(columns=colomns_name)
                m_t = m_t+1
                print('end_of {} {}'.format(assetID, m_t))
            m_t = 0
            print('new_asset')

client = authenticate(DEFAULT_SETTINGS_FILE)
plants = getPlants(client)
assets = getAssets(plants)
print(assets)
#getMeasurementNames(assets[17:55])
df=createDataFrame(assets)


